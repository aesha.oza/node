//const http = require('http');
//const fs = require('fs');
//const routes = require('./routes');
//console.log(routes.someText);

//import { create } from 'express-handlebars';

const path = require('path');
const express = require('express'); 
const bodyParser = require('body-parser');
//const expressHbs = require('express-handlebars');
const mongoose = require('mongoose');
const session = require('express-session');
const mongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');

const errorController = require('./controllers/error');
const mongoConnect = require('./util/database').mongoConnect;

const User = require('./models/user');

const MONGODB_URI = 'mongodb+srv://Aesha:guddi%402001@cluster0.ftwg9d8.mongodb.net/shop';

/* Mysql database connection
const sequelize = require('./util/database');
const Product = require('./models/product');
const User = require('./models/user');
const Cart = require('./models/cart');
const CartItem = require('./models/cart-item');
const Order = require('./models/order');
const OrderItem = require('./models/order-item');*/


const app = express();
const store = new mongoDBStore({
    uri : MONGODB_URI,
    collection: 'sessions'
});

const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
    destination:(req,file,cb)=>{
        cb(null,'images');
    },
    filename:(req,file,cb)=>{
        cb(null, new Date().toISOString() + '-' + file.originalname);
    }
});

const fileFilter = (req,file,cb)=>{
    if(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg'){
        cb (null,true);
    }else{
        cb(null,false);
    }
};

//app.engine('hbs',expressHbs.engine({layoutsDir: 'views/layouts/',defaultLayout: 'main-layout',extname:'hbs'}));
app.set('view engine','ejs');
app.set('views','views');

const adminRoutes = require('./routes/admin');
// const adminData = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');
const { error } = require('console');

//const { getMaxListeners } = require('process');

// db.execute('SELECT * FROM products')
// .then((result) => {
//     console.log(result[0],result[1]);
// })
// .catch((err) => {
//     console.log(err);
// }); --->> testing purpose

app.use(bodyParser.urlencoded({extended: false}));
app.use(multer({storage:fileStorage, fileFilter:fileFilter}).single('image'));
app.use(express.static(path.join(__dirname,'public')));
app.use('/images',express.static(path.join(__dirname,'images')));

app.use(
    session({
        secret: 'my secret', 
        resave:false , 
        saveUninitialized:false,
        store: store
    })
);

app.use(csrfProtection);
app.use(flash());

 app.use((req,res,next)=> {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.use((req,res,next)=>{
    if(!req.session.user){
        return next();
    }
    User.findById(req.session.user._id)
    //User.findById('63ecd0c230309667bbb99080')
    .then(user => {
        //req.user = new User(user.name,user.email,user.cart,user._id);
        if(!user){
            return next();
        }
        req.user= user;
        next();
    })
    .catch(err => {
        next(new Error(err));
    });
});


app.use('/admin',adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.get('/500',errorController.get500);

app.use(errorController.get404);

app.use((error,req,res,next)=> {
    // res.status(error.httpStatusCode).render();
    // res.redirect('/500');
    res.status(500).render('500',{
        pageTitle:'Error!',
        path: '/500',
        isAuthenticated : req.session.isLoggedIn
    });
});
//app.use(express.json());

/*  mongodb connect
mongoConnect(() => {
    //console.log(client);
    app.listen(3000, () =>{
        console.log("server running");
    });
});*/

mongoose.set('strictQuery', true);
mongoose
    .connect(MONGODB_URI)
    .then(result => {
        // User.findOne().then(user => {
        //     if(!user){
        //         const user = new User({
        //             name : 'Jack',
        //             email : 'jack@gmail.com',
        //             cart:{
        //                 items:[]
        //             }
        //         });
        //         user.save();
        //     }
        // })
        app.listen(3005, () =>{
            console.log("server running");
        });
    })
    .catch(err => {
        console.log(err);
    });



/*app.use((req,res,next)=>{
    //res.status(404).send('<h1>page not found</h1>');
    //res.status(404).sendFile(path.join(__dirname,'views','404.html'));
    res.status(404).render('404',{pageTitle:'page not found'});
});*/

// app.use((req,res,next)=> {
//     console.log('in the middleware');
//     next(); //allow the request to continue to the next middleware in line
// });
// app.use('/add-product',(req,res,next)=> {
//     //console.log('in another middleware');
//     res.send('<form action="/product" method="POST"><input type="text" name="title"><button type="submit">submit</button></form>');
// });
// app.use('/product',(req,res,next)=>{
//     console.log(req.body);
//     res.redirect('/');
// });
// app.use('/',(req,res,next)=> {
//     //console.log('in another middleware');
//     res.send('<h1>Hello expresss!</h1>');
// });

/* Mysql --> database connection
Product.belongsTo(User,{constraints: true , onDelete: 'cascade'});
User.hasMany(Product);
User.hasOne(Cart);
Cart.belongsTo(User);
Cart.belongsToMany(Product, {through : CartItem});
Product.belongsToMany(Cart , {through : CartItem});
Order.belongsTo(User);
User.hasMany(Order);
Order.belongsToMany(Product,{through:OrderItem});

sequelize
    .sync({force : true})
    //.sync()
    .then(result => {
       // console.log(result);
        //app.listen(3000);
        return User.findByPk(1);
    })
    .then(user=> {
        if (!user){
            return User.create({name:'Max',email:'abc@gmail.com'});
        }
        return user;
    })
    .then(user => {
        //console.log(user);
        return user.createCart();
    })
    .then(cart => {
        app.listen(3000);
    })
    .catch(err => {
        console.log(err);
    });
*/


//app.listen(3000);


//const server = http.createServer(app);
//const server = http.createServer(routes.handler);
//const server = http.createServer((req , res)=>{
    //console.log(req);
    //req---->>>  console.log(req.url,req.method,req.headers);
    //process.exit();

    // const url = req.url;
    // const method = req.method;
//     if(url === '/'){
//         res.write('<html>');
//         res.write('<head><title>enter message</title></head>');
//         res.write('<body><form action="/message" method="POST"><input type="text" name="mesage"><button type="submit">send</button></form></body>');
//         res.write('</html>');
//         return res.end();
//     }
//     if(url === '/message' && method === 'POST'){
//         const body = [];
//         req.on('data',(chunk) => {
//             console.log(chunk);
//             body.push(chunk);
//         });
//         req.on('end',() => {
//             const parseBody = Buffer.concat(body).toString();
//             //console.log(parseBody);
//             const message = parseBody.split('=')[1];
//             // fs.writeFileSync('message.txt',message);
//             // res.statusCode=302;
//             // res.setHeader('Location','/');
//             // return res.end();
//             fs.writeFileSync('message.txt',message, err => {
//                 res.statusCode=302;
//             res.setHeader('Location','/');
//             return res.end();
//             });    
//         });
//         //fs.writeFileSync('message.txt','DUMMY');
//         // res.statusCode=302;
//         // res.setHeader('Location','/');
//         // return res.end();
//     }
//     res.setHeader('content-type','text/html');
//     res.write('<html>');
//     res.write('<head><title>My first page</title></head>');
//     res.write('<body><h1>Hello!!</h1></body>');
//     res.write('</html>');
//     res.end();
// });
//server.listen(3000);