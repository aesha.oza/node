const Product = require('../models/product')

exports.getAddProduct = (req,res,next)=> {
    res.render('admin/add-product',{
        pageTitle: 'add product', 
        path:'/admin/add-product',
    });
};

exports.postAddProduct = (req,res,next)=>{
    const product = new Product(req.body.title);
    product.save(); 
    res.redirect('/');
};

exports.getProducts= (req,res,next)=> {
    Product.fetchAll(products =>{
        res.render('./shop/product-list',{
            prods: products , 
            pageTitle:'shop' , 
            path : '/',
            hasProducts: products.length > 0,
            activeShop: true,
            productCSS:true
        });
    }); 
};